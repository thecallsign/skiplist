use std::rc::Rc;
use std::cell::RefCell;
type RcNode<T> = Rc<RefCell<Node<T>>>;

struct Node<T> {
    data: T,
    next: Option<RcNode<T>>,
    prev: Option<RcNode<T>>
}

impl<T> Node<T> {
    pub fn new(data: T) -> Self {
        Self {
            data,
            next: None,
            prev: None
        }
    }
}

impl<T> Node<T> {
    pub fn next_mut(&mut self) -> Option<RcNode<T>> {
        self.next.clone()
    }

    pub fn prev_mut(&mut self) -> Option<RcNode<T>> {
        self.prev.clone()
    }
}

struct Tree<T> {
    head: Option<Node<T>>
}

impl<T> Tree<T> {
    pub fn new() -> Self {
        Self {
            head: None
        }
    }

    pub fn push(&mut self, item: T) {
        if let Some(last) = self.last_mut() {
            last.next = Some(Node::new(item).into())
        } else {
            self.head = Some(Node {
                data: item,
                next: None,
                prev: None
            })
        }
    }

    pub fn pop() {

    }

    pub fn last_mut(&mut self) -> Option<&mut Node<T>> {
        let mut next = self.head.as_mut();
        while let Some(current) = next {
            next = current.next_mut()
        }
        next
    }
}

fn main() {

}
